import os
import picamera
import time

DIR = os.getcwd() + "/images/captured_images"

# Create caputre directory if it doesn't exist
if not os.path.exists(DIR):
    os.makedirs(DIR)
    print("No directory - so it was created, look in images/captured_images for your images!")
else:
    print("Directory found, captured images will be stored in images/captured_images!")

camera = picamera.PiCamera()
camera.resolution = (224,224)
camera.rotation = 180

print("About to start capturing images, press Ctrl-C to stop")
counter = 1
while True:
    camera.start_preview()
    time.sleep(3)
    filename = DIR + "/" + str(int(time.time())) + ".jpg"
    camera.capture(filename)
    print("Image " + str(counter) + " saved to " + filename)
    counter += 1
    camera.stop_preview()
